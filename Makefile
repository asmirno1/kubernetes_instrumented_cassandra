SHELL=/bin/bash
CASSANDRA_REPLICAS=3

build:
	./scripts/build_images.sh
start: build
	kubectl apply -k kubernetes_configuration
	sleep 1
	$$(kubectl wait --for=condition=Ready --timeout 5m pod -l app=zipkin && kubectl port-forward service/zipkin 9411:9411) &\
	$$(kubectl wait --for=condition=Ready --timeout 5m pod cassandra-0 && kubectl wait --for=condition=Ready --timeout 40s pod -l app=cassandra-web && kubectl port-forward service/cassandra-web 3000:3000) &\
	$$(kubectl wait --for=condition=Ready --timeout 5m pod cassandra-0 && kubectl scale statefulset cassandra --replicas $(CASSANDRA_REPLICAS));
stop:
	# kubectl delete deployment casstrace-zipkin
	kubectl delete -k kubernetes_configuration --all
force_stop: stop
	@kill $$(lsof -t -i:9411) || echo "Nothing on 9411"
	@kill $$(lsof -t -i:3000) || echo "Nothing on 3000"