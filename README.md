# Zipkin-Instrumented Cassandra Setup on Kubernetes

### Prerequisite installs

- [Kubernetes](https://kubernetes.io/) Cluster
    - In OSX or Windows, enable through the App
        - e.g. Preferences -> Kubernetes -> Enable Kubernetes
    - Can also install using [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) or [Kind](https://kind.sigs.k8s.io/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [GNU-Make](https://www.gnu.org/software/make/)

### How to use:

1. `make start` to spin up the 3-replica Kubernetes Cassandra cluster and other services.
    - `make start CASSANDRA_REPLICAS=<NUM_DESIRED_REPLICAS>` for a different amount of replicas.
        - This make take some time.
    - Monitor progress with `kubectl get pods --watch`
    - The `casandra-web` pod will repeatedly fail to start until the first cassandra instance is ready (you may see 2-3 restarts).
    - The complete Casandra cluster will take a while to instantiate (about 30 seconds per replica)
    - If some process is using ports `3000` or `9411` on your local machine, the interface to Cassandra and Zipkin will not be able to bind. In this case, run `make force_stop` to kill the processes using those ports.
    - `Ctrl-C/Cmd-C` terminates the foreground logging for the web services, but they will still be operational until stopped with `make stop`.
2. Go to `http://localhost:3000` for a UI to the Cassandra cluster's `cqlsh`.
    - Click "Execute" in the top-right corner of the window to execute commands.
    - *Important:* There is a bug with WebUI/ Tracing that prevents the execution window from responding after executing a comand with tracing.
        - The command will still be executed.
        - Close the window and reopen it to execute another command if desired.
3. Visit `http://localhost:9411` to view traces.
4. `make stop` to stop and remove the Kubernetes resources.

#### Sample commands

```
CREATE KEYSPACE test
    WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor': 3 };
```
```
CREATE TABLE test.testtable (
    test_id varchar primary key,
    test_data varchar);
```
```
INSERT INTO test.testtable(test_id, test_data) VALUES ('ID_1', 'hello world'); 
```

### Current Issues

Can use `podManagementPolicy: Parallel` in the statefulset configuration to bring up all nodes, but they trip over each other with IP addresses and force restarts. CrashLoopBackoff then makes sure that they won't be up quickly.
- Can use the followitg code for random jitter, but it is not very helpful unless large:

```
    initContainers:
- name: random-jitter
    image: busybox:1
    command: ["/bin/sh", "-c", "sleep $(($RANDOM % 120))"]
```

#### Github Issues to Track

[Proposal: allow StatefulSet to use RestartPolicy "OnFailure" and "Never"](https://github.com/kubernetes/community/issues/458)
[Make CrashLoopBackoff timing tuneable, or add mechanism to exempt some exits](https://github.com/kubernetes/kubernetes/issues/57291)
[Backoffs should have jitter](https://github.com/kubernetes/kubernetes/issues/87915)

#### Debugging Notes

- Do not add prefix/ suffix to services, DNS does not resolve for cassandra.
- Cassandra instances require memory limits. Without setting this, some instances will begin to crash after a certain number of instances are up.

### TODO:

- Cassandra instance 0 needs to be up before the others, but all others can be created simultaneously. This would speed up how quickly a cluster can be created.
