#!/bin/bash

kind delete cluster
kind create cluster --wait 50s --config /setup/kind_config.yaml
kind export kubeconfig
kind load docker-image busybox:1
