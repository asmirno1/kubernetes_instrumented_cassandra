#!/bin/bash

shopt -s nullglob
for build_dir in services/*
do
    if [ -f "$build_dir/Dockerfile" ]; 
    then
        imageName=$(basename $build_dir)
        docker build -t $imageName $build_dir
        if kind get clusters | grep -q kind; # Note: only works with default kind cluster
        then
            kind load docker-image $imageName
        fi
    fi
done;